# README #

## Info
Repozytorium przechowujące testową wersję projektu AngularGT - strony typu SinglePageApp w technologii Angular2 skierowanej do diabetyków, lekarzy- diabetologów i osób chcących z jakichkolwiek powodów kontrolować swój stan zdrowia w perspektywie gospodarki węglowodanowej organizmu.

## Cel
Stosując Angulara2 i Typescripta stworzyć stronę pozwalającą przetestować framework w zastosowaniu gratyfikacji w leczeniu chorób przewlekłych wymagających współpracy z lekarzem.
Celem nie jest powielanie istniejących rozwiązań (choć również nie przeciwstawianie się im), lecz sprawdzenie, czy istnieje możliwość lepszego zrozumienia tematu przy wykorzystaniu technologii pozwalającej tworzyć singlepageapp modularnie i proceduralnie. Zbierane dane mają posłużyć stworzeniu podstaw do szerszego projektu.

## Typy użytkowników
* Stan przedcukrzycowy
* Typ I
* Typ II
* Typ ciążowy
* MODO
* LADA
* Lekarz
* Osoba wspierająca

## Metody
* Angular2
* TDD (opcjonalnie)
* Karma (opcjonalnie)
* JS
* DataDrivenJavascript (w następnej wersji)
* Gratyfikacja
* Meanstack
* MongoDB
* Node
* CLI

## Jak uruchomić?
* Na serwerze skonfigurowany MongoDB
* Uruchomić serwer MongoDB
* Uruchomić konsole serwera MongoDB 
* Uruchomić serwer poleceniem node z głównego katalogu aplikacji
* Zbudować i uruchomić aplikacje 

- Uwaga, robiąc npm update by pobrac dependency należy zauważyć, że cześć z nich jest niepotrzebna i wchodzi z konflikt z innymi. Gdyby nie wchodziły, to przynajmniej by nie przeszkadzały, tak,
wiedząc, że nie można wrzucać ich na gita, muszę ostrzeć, że trzeba na to uważać i musiałem przez to nadużywać przecinków.
I tak, sourcetree powinno mieć w commicie doesn't a nie don't.